
'use strict';

var hapi = require('hapi');
var corsHeaders = require('hapi-cors-headers');
var axios = require('axios');

var config = {
  server: {
    host: '127.0.0.1',
    port: 3010
  }
}

var server = new hapi.Server();
server.connection({
  host: config.server.host,
  port: config.server.port	
});

server.ext('onPreResponse', corsHeaders);

server.start(function() {
  var date = new Date();
  var time = date.getHours().toString().padStart(2,"0") + ":" + 
    date.getMinutes().toString().padStart(2,"0") + ":" + 
    date.getSeconds().toString().padStart(2,"0");
  console.log(time + " | Server listening on:", server.info.uri)
});

// ####################################### PRUEBA QPAY PRO #######################################

server.route({
  method: 'GET',
  path: '/qpaypro-test',
  config: {
    handler: qPayProTest
  }
});

function qPayProTest(request, reply){
	
	let URL = 'https://sandbox.qpaypro.com/payment/api_v1';
	let jsonValues = {
			"x_login": "visanetgt_qpay", 
			"x_private_key": "88888888888", 
			"x_api_secret": "99999999999", 
			"x_product_id": "1", 
			"x_audit_number": "20", 
			"x_fp_sequence": "20", 
			"x_fp_timestamp": "154681351", 
			"x_invoice_num": "001", 
			"x_currency_code": "GTQ", 
			"x_amount": "50.00", 
			"x_line_item": "T-shirt Live Dreams<|>w01<|><|>1<|>85.00<|>N", 
			"x_freight": "0", 
			"x_email": "email@comapny.com", 
			"cc_number": "4000000000000242", 
			"cc_exp": "12/19", // formulario
			"cc_cvv2": "4567", // formulario
			"cc_name": "Pedro Quino",  // formulario
			"x_first_name": "Pedro",  // formulario
			"x_last_name": "Quino",  // formulario
			"x_company": "12345678", 
			"x_address": "1 calle 2 Ave", // formulario
			"x_city": "Guatemala",  // formulario
			"x_state": "Guatemala",  // formulario
			"x_country": "Guatemala",  
			"x_zip": "01001", 
			"x_relay_response": "none", 
			"x_relay_url": "none", 
			"x_type": "AUTH_ONLY", 
			"x_method": "CC", 
			"http_origin": "tuempresa.com", 
			"cc_type": "visa", // formulario
			"visaencuotas": 0, 
			"device_fingerprint_id":"5c6b565edd409"
		};
	var headers = {
        'Content-Type': 'application/json;charset=UTF-8'
    }
	axios.post(URL, jsonValues, headers)
	.then(function (response) {
		console.log(response.data);
	})
	.catch(function (error) {
		console.log(error);
	});
	
	// console.log(jsonValues);

	
	reply({ code: 1, message: "Prueba QPayPro completada exitosamente" });
	
}


// ####################################### TEST GET REQUEST #######################################

server.route({
  method: 'GET',
  path: '/test-get',
  config: {
    handler: testGet
  }
});


function testGet(request, reply){
		
	reply({ code: 1, message: "GET request completed succcessfully" });
	
}

















