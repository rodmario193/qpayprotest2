

var departamentos = [
	{  nombre: "Escuintla",
	   municipios: [
			{ nombre: "Escuintla" },
			{ nombre: "La Gomera" },
			{ nombre: "San Jos�" },
			{ nombre: "Tiquisate" },
			{ nombre: "Guanagazapa" },
			{ nombre: "Masagua" },
			{ nombre: "San Vicente Pacaya" },
			{ nombre: "Iztapa" },
			{ nombre: "Nueva Concepci�n" },
			{ nombre: "Santa Luc�a Cotzumalguapa" },
			{ nombre: "La Democracia" },
			{ nombre: "Pal�n" },
			{ nombre: "Siquinal�" }
	   ]
	},
	{  nombre: "Guatemala" 
	   municipios: [
			{ nombre: "Amatitl�n" },
			{ nombre: "Guatemala" },
			{ nombre: "San Jos� Pinula" },
			{ nombre: "San Pedro Sacatep�quez" },
			{ nombre: "Villa Nueva" },
			{ nombre: "Chinautla" },
			{ nombre: "Mixco" },
			{ nombre: "San Juan Sacatep�quez" },
			{ nombre: "San Raymundo" },
			{ nombre: "Chuarrancho" },
			{ nombre: "Palencia" },
			{ nombre: "San Miguel Petapa" },
			{ nombre: "Santa Catarina Pinula" },
			{ nombre: "Fraijanes" },
			{ nombre: "San Jos� del Golfo" },
			{ nombre: "San Pedro Ayampuc" },
			{ nombre: "Villa Canales" }
	   ]
	},
	{  nombre: "Izabal", 
	   municipios: [
			{ nombre: "El Estor" },
			{ nombre: "Puerto Barrios" },
			{ nombre: "Livingston" },
			{ nombre: "Los Amates" },
			{ nombre: "Morales" }
		]
	},
	{  nombre: "Quetzaltenango",
	   municipios: [
			{ nombre: "Almolonga" },
			{ nombre: "Coatepeque" },
			{ nombre: "Flores Costa Cuca" },
			{ nombre: "Olintepeque" },
			{ nombre: "San Carlos Sija" },
			{ nombre: "San Mateo" },
			{ nombre: "Cabric�n" },
			{ nombre: "Colomba" },
			{ nombre: "G�nova" },
			{ nombre: "Palestina de Los Altos" },
			{ nombre: "San Francisco La Uni�n" },
			{ nombre: "San Miguel Sig�il�" },
			{ nombre: "Cajol�" },
			{ nombre: "Concepci�n Chiquirichapa" },
			{ nombre: "Huit�n" },
			{ nombre: "Quetzaltenango" },
			{ nombre: "San Juan Ostuncalco" },
			{ nombre: "Sibilia" },
			{ nombre: "Cantel" },
			{ nombre: "El Palmar" },
			{ nombre: "La Esperanza" },
			{ nombre: "Salcaj�" },
			{ nombre: "San Mart�n Sacatep�quez" },
			{ nombre: "Zunil" }
		]
	}
];













